import { Injectable } from '@angular/core';
import { Leader } from '../shared/leader';
import { LEADERS } from '../shared/leaders';
import { Observable ,of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { map, catchError } from 'rxjs/operators';
import { ProcessHTTPMsgService } from './process-httpmsg.service';

@Injectable({
  providedIn: 'root'
})
export class LeaderService {

  constructor( private http: HttpClient, private processHTTPMsgService: ProcessHTTPMsgService ) {  }
  
  // promise based methods
  // getLeaders(): Promise<Leader[]>{
  //   return new Promise( resolve => {
  //     // simulate server delay with 2 sec delay
  //     setTimeout(() => resolve(LEADERS), 1000);
  //   });
  // }

  // getLeader(id: string): Promise<Leader>{
  //   return new Promise( resolve => { 
  //     setTimeout(() => resolve(LEADERS.filter((leader) => (leader.id === id))[0]), 1000);    
  //   });
  // }

  // getFeaturedLeader(): Promise<Leader>{
  //   return new Promise( resolve => {
  //     setTimeout(() => resolve(LEADERS.filter((leader) => leader.featured)[0]), 1000);
  //   });
  // }

  //observable based methods
  getLeaders(): Observable<Leader[]>{
    // return of(LEADERS).pipe(delay(1000));
    return this.http.get<Leader[]>(baseURL + 'leadership')
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getLeader(id: string): Observable<Leader>{
    // return of(LEADERS.filter((leader) => (leader.id === id))[0]).pipe(delay(1000));
    return this.http.get<Leader>(baseURL + 'leadership/' + id)
      .pipe(catchError(this.processHTTPMsgService.handleError));   
  }

  getFeaturedLeader(): Observable<Leader>{
    // return of(LEADERS.filter((leader) => leader.featured)[0]).pipe(delay(1000));
    return this.http.get<Leader[]>(baseURL + 'leadership?featured=true')
      .pipe(map(leaders => leaders[0]))
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }
}
