import { Injectable } from '@angular/core';
import { Dish } from '../shared/dish';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { map, catchError } from 'rxjs/operators';
import { ProcessHTTPMsgService } from './process-httpmsg.service';

@Injectable({
  providedIn: 'root'
})
export class DishService {

  constructor( private http: HttpClient, private processHTTPMsgService: ProcessHTTPMsgService ) { }
 
  // Synchronous methods:
  // getDishes(): Dish[]{
  //   return DISHES;
  // }

  // getDish(id: string): Dish{
  //   return DISHES.filter((dish) => (dish.id === id))[0];
  // }

  // getFeaturedDish(): Dish{
  //   return DISHES.filter((dish) => dish.featured)[0];
  // }


  // promise based methods
  // getDishes(): Promise<Dish[]>{
  //   // for immediate resolve
  //   // return Promise.resolve(DISHES);
  //   // promise resolved with 2sec delay 
  //   return new Promise( resolve => {
  //     // simulate server delay with 2 sec delay
  //     setTimeout(() => resolve(DISHES), 1000);
  //   });
  // }

  // getDish(id: string): Promise<Dish>{
  //   // for immediate resolve
  //   // return Promise.resolve(DISHES.filter((dish) => (dish.id === id))[0]);
  //   // promise resolved with 2sec delay 
  //   return new Promise( resolve => { 
  //     setTimeout(() => resolve(DISHES.filter((dish) => (dish.id === id))[0]), 1000);    
  //   });      
  // }
  
  // getFeaturedDish(): Promise<Dish>{
  //   // for immediate resolve
  //   // return Promise.resolve(DISHES.filter((dish) => dish.featured)[0]);
  //   // promise resolved with 2sec delay
  //   return new Promise( resolve => {
  //     setTimeout(() => resolve(DISHES.filter((dish) => dish.featured)[0]), 1000);
  //   });
  // }

  //observable based methods
  // getDishes(): Observable<Dish[]>{
  //     return of(DISHES).pipe(delay(1000));
  // }; 
    
  // getDish(id: string): Observable<Dish>{
  //   return of(DISHES.filter((dish) => (dish.id === id))[0]).pipe(delay(1000));    
  // };
  
  // getFeaturedDish(): Observable<Dish>{
  //   return of(DISHES.filter((dish) => dish.featured)[0]).pipe(delay(1000)); 
  // };

  // getDishIds(): Observable<string[] | any> {
  //   return of(DISHES.map(dish => dish.id));
  // }

  // obtain data with HTTP client from the server
  getDishes(): Observable<Dish[]>{
      return this.http.get<Dish[]>(baseURL + 'dishes')
        .pipe(catchError(this.processHTTPMsgService.handleError));
  }; 
    
  getDish(id: string): Observable<Dish>{
    return this.http.get<Dish>(baseURL + 'dishes/' + id)
      .pipe(catchError(this.processHTTPMsgService.handleError));;
  };
  
  getFeaturedDish(): Observable<Dish>{
    return this.http.get<Dish[]>(baseURL + 'dishes?featured=true')
      .pipe(map(dishes => dishes[0]))
      .pipe(catchError(this.processHTTPMsgService.handleError));;
  };

  getDishIds(): Observable<string[] | any> {
    return this.getDishes()
      .pipe(map(dishes => dishes.map(dish => dish.id)))
      .pipe(catchError(error => error));
  }

  putDish(dish: Dish): Observable<Dish> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.put<Dish>(baseURL + 'dishes/' + dish.id, dish, httpOptions)
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }
}

