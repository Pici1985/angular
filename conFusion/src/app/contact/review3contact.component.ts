import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { flyInOut, expand } from '../animations/app.animation';
import { Feedback, ContactType } from '../shared/feedback'
import { FeedbackService } from '../services/feedback.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut(),
    expand()
  ]
})
export class ContactComponent implements OnInit {

  feedbackForm!: FormGroup
  feedback!: Feedback
  contactType = ContactType
  @ViewChild('fform') feedbackFormDirective!: any
  submitErrMsg!: string
  submitting!: boolean
  showForm!: boolean

  formErrors: any = {
    'firstname': '',
    'lastname': '',
    'telnum': '',
    'email': ''
  }

  validationMessages: any = {
    'firstname': {
      'required': 'First name is required',
      'minlength': 'First name must be at least 2 chars long',
      'maxlength': 'First name cannot be more than 25 chars'
    },
    'lastname': {
      'required': 'Last name is required',
      'minlength': 'Last name must be at least 2 chars long',
      'maxlength': 'Last name cannot be more than 25 chars'
    },
    'telnum': {
      'required': 'Tel num is required',
      'pattern': 'Tel num must contain only digits'
    },
    'email': {
      'required': 'Email is required',
      'email': 'Email format invalid'
    }
  }

  constructor(
    private fb: FormBuilder,
    private feedbackService: FeedbackService
  ) {
    this.createForm()
    this.showForm = true
    this.submitting = false
  }

  ngOnInit() {
  }

  createForm = () => {
    this.feedbackForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      telnum: ['', [Validators.required, Validators.pattern]],
      email: ['', [Validators.required, Validators.email]],
      agree: false,
      contacttype: 'None',
      message: ['', Validators.required],
    })

    this.feedbackForm.valueChanges
      .subscribe(data => this.onValueChanged(data))

    this.onValueChanged() //(re)set form validation messages
  }

  onValueChanged(data?: any) {
    if (!this.feedbackForm) return;
    const form = this.feedbackForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear prev error message
        this.formErrors[field] = ''
        const control = form.get(field)
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field]
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' '
            }
          }
        }
      }
    }
  }

  onSubmit = () => {
    const feed = this.feedbackForm.value
    this.showForm = false
    this.submitting = true
    this.feedbackService.submitFeedback(feed)
      .subscribe(
        (feedback) => {
          this.feedback = feedback
          this.submitting = false
          setTimeout(() => this.showForm = true, 5000)
        },
        (err) => {
          this.submitErrMsg = err
          this.submitting = false
          setTimeout(() => this.showForm = true, 5000)
        }
      )
    this.feedbackForm.reset(new Feedback()) // moved default values to constructor
    this.feedbackFormDirective.resetForm(new Feedback())
  }

}
