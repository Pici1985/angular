import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Feedback, ContactType } from '../shared/feedback';
import { FeedbackserviceService } from '../services/feedbackservice.service';



 
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  feedbackForm!: FormGroup;
  feedback!: Feedback;
  contactType = ContactType;
  
  @ViewChild('fform') feedbackFormDirective!: any;
  
  formErrors: any = {
    'firstname' : '',
    'lastname' : '',
    'telnum' : '',
    'email' : ''
  };

  validationMessages: any = {
    'firstname': {
      'required': 'First name is required.',
      'minlength': 'First name must be at least 2 characters long.',
      'maxlength': 'First name cannot be more than 25 characters'
    },
    'lastname': {
      'required': 'Last name is required.',
      'minlength': 'Last name must be at least 2 characters long.',
      'maxlength': 'Last name cannot be more than 25 characters'
    },
    'telnum': {
      'required': 'Phone number is required.',
      'pattern': 'Phone number must only contain numbers.' 
    },
    'email': {
      'required': 'Email is required.',
      'pattern': 'Email not in valid formnat.' 
    }
  };

  feedbackcopy!: Feedback | null; 

  constructor(private fb: FormBuilder,
              public feedbackservice: FeedbackserviceService,
              @Inject('baseURL') public baseURL: any) {
    this.createForm();
  }
  
  ngOnInit(): void {
    
  }

  createForm() {
    this.feedbackForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      telnum: [null, [Validators.required, Validators.pattern ]],
      email: ['', [Validators.required, Validators.email ]],
      agree: false,
      contacttype: 'None',
      message: ''
    });
    // console.log(this.feedbackForm);

    this.feedbackForm.valueChanges.subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // reset form validation messages
  }

  onValueChanged(data?: any){
    if (!this.feedbackForm ) { return; }
    const form = this.feedbackForm;
    for (const field in this.formErrors){
      if(this.formErrors.hasOwnProperty(field)){
        //clear previous error message (if any)
        this.formErrors[field]= '';
        const control = form.get(field);
        if(control && control.dirty && !control.valid){
          const messages = this.validationMessages[field];
          for (const key in control.errors){
            if(control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';  
            }
          }
        }
      }
    } 
  }
  
  onSubmit() {
    this.feedback = this.feedbackForm.value;
    // console.log(this.feedback);
    this.feedbackForm.reset({
      firstname: '',
      lastname: '',
      telnum: '',
      email: '',
      agree: false,
      contacttype: 'None',
      message: ''
    });
    this.feedbackFormDirective.resetForm();    
  }

  submitFeedback(){
    this.feedback = this.feedbackForm.value;
    this.feedbackservice.postFeedback(this.feedback)
    .subscribe(feedback => this.feedback = feedback );
    this.feedbackcopy = this.feedback;
    if(this.feedbackcopy){
      setTimeout(() => { this.feedbackcopy = null }, 5000 )
      console.log(this.feedbackcopy);
    }
  }
};
