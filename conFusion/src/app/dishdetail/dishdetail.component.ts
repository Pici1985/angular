import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { switchMap } from 'rxjs/operators';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Comment } from '../shared/comment';

import { formatDate } from '@angular/common';

import { visibility } from '../animations/app.animation';
// import { trigger, state, style, animate, transition } from '@angular/animations';






// import { baseURL } from '../shared/baseurl'; 


@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  animations: [
    visibility()
  ]
  //   trigger('visibility', [
  //     state('shown', style({
  //       transform: 'scale(1.0)',
  //       opacity: 1
  //     })),
  //     state('hidden', style({
  //       transform: 'scale(0.5)',
  //       opacity: 0
  //     })),
  //     transition('* => *', animate('2s ease-in-out'))
  //   ])
  // ]
})

export class DishdetailComponent implements OnInit {

  // @Input()  
  dish!: Dish;
  dishIds!: string[];
  prev!: string;
  next!: string;
  errMess!: string;
  visibility = 'shown';

  // this is my code from here
  @ViewChild('ratingform') ratingFormDirective!: any;
  
  // this is for the slider to show thumbnails
  thumbLabel = true;

  // creating variables
  ratingForm!: FormGroup;
  comment!: Comment;

  //for persisiting data to the server
  dishcopy!: Dish; 

   // my code until here

  constructor(private dishService: DishService,
              private location: Location, 
              private route: ActivatedRoute, 
              private fb: FormBuilder,
              @Inject('baseURL') public baseURL: any) {
      this.createForm();
    }

  ngOnInit(): void {
    this.dishService.getDishIds()
      .subscribe((dishIds) => this.dishIds = dishIds );
    this.route.params
      .pipe(switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishService.getDish(params['id']); }))
      .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; }, 
        errmess => this.errMess = <any>errmess);
  }

  
  setPrevNext(dishId: string){
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1 ) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1 ) % this.dishIds.length];    
  }
  
  goBack(): void{
    this.location.back();
  }
  
  
  // my code from here 
  createForm() {
    this.ratingForm = this.fb.group({
      author: ['', [Validators.required,Validators.minLength(2)]],
      rating: ['5', [Validators.required]],
      comment: ['', [Validators.required]]
    });
  }
    // this is where I`m getting the date 
    currentDate = formatDate(new Date(), 'yyyy/MM/dd', 'en');
    
    // this is the submit method
    ratingFormSubmitted() {
      this.comment = this.ratingForm.value;
     
      this.comment.date = this.currentDate.toString();
      // console.log(this.comment);      
      this.dishcopy.comments.push(this.comment);
      this.dishService.putDish(this.dishcopy)
        .subscribe(dish => {
          this.dish = dish; this.dishcopy = dish; 
        },
        errmess => {this.dish = <any>errmess; this.dishcopy = <any>errmess; this.errMess = <any>errmess; });
      this.ratingForm.reset({
        author: '',
        rating: '5',
        comment: ''
      });
    }

    // until here
}  
